package main

import "testing"

func TestSum(t *testing.T)  {
	a := 1 + 1
	if a != 2 {
		t.Errorf("Výsledek je %d, očekáváno 2", a)
	}
}