FROM golang:1.12-alpine AS build_base

RUN apk add --no-cache git

WORKDIR /tmp/rest-api-app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 go test -v

RUN go build -o ./out/rest-api-app .


FROM alpine:3.9
RUN apk add ca-certificates

COPY --from=build_base /tmp/rest-api-app/out/rest-api-app /app/rest-api-app

EXPOSE 8080

CMD ["/app/rest-api-app"]

